/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.weka.gui;

import weka.clusterers.HierarchicalClusterer;
import weka.gui.explorer.ClustererPanel;

/**
 *
 * @author lendle
 */
public class HierarchicalClustererPanel extends ClustererPanel{
    public HierarchicalClustererPanel() {
        m_ClustererEditor.setValue(new HierarchicalClusterer());
    }
}
